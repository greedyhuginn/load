@setlocal enableextensions
@cd /d "%~dp0"

net start w32time
w32tm /resync /force

ren 7z*.exe1 7z.exe
start /wait 7z.exe /S 

set PATH=%PATH%;"c:\Program Files\7-Zip"
del 7z.exe

7z e curl*.zip -oc:\curl
7z x load.zip

start /wait "Firefox" "Firefox Installer.exe" -ms
start /wait TeamViewer_Setup.exe /S /norestart
start /wait ProxifierSetup.exe /VERYSILENT /SUPPRESSMSGBOXES /NORESTART /SP-

regedit /s "proxifier activator.reg"

start /wait firewall.bat

proxy.ppx