chcp 1251 >NUL

netsh advfirewall firewall add rule name="Proxifier" dir=out action=allow program="C:\Program Files (x86)\Proxifier\Proxifier.exe"
netsh advfirewall firewall add rule name="ProxyChecker" dir=out action=allow program="C:\Program Files (x86)\Proxifier\ProxyChecker.exe"

netsh advfirewall firewall add rule name="TeamViewer" dir=out action=allow program="C:\Program Files (x86)\TeamViewer\TeamViewer.exe"
netsh advfirewall firewall add rule name="TeamViewer_Service" dir=out action=allow program="C:\Program Files (x86)\TeamViewer\TeamViewer_Service.exe"
netsh advfirewall firewall add rule name="TeamViewer_Desktop" dir=out action=allow program="C:\Program Files (x86)\TeamViewer\TeamViewer_Desktop.exe"

netsh advfirewall set publicprofile firewallpolicy blockinbound,blockoutbound
netsh advfirewall set privateprofile firewallpolicy blockinbound,blockoutbound
netsh advfirewall set domainprofile firewallpolicy blockinbound,blockoutbound

netsh interface ipv4 set dnsservers "����������� �� ��������� ����" static 1.1.1.1 primary no
netsh interface ipv4 add dnsservers "����������� �� ��������� ����" 1.0.0.1 index=2 validate=no

exit